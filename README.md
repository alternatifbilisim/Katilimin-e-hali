#Katılımın "e-hali": Gençlerin Sanal Alemi
**Katılımın e-hali: Gençlerin Sanal Alemi**, Türkiye'de gençlik alanında internetin sivil ve politik katılıma etkisini masaya seren ilk e-derleme. Avrupa Komisyonu VI. Çerçeve Programı tarafından yurttaşlık ve yönetişim teması altında 2006-2009 yılları arasında fonlanan **Civicweb: İnternet, gençlik ve sivil/politik katılım** (Civicweb: Internet, youth and civic participation: http://civicweb.eu/)" başlıklı çok ortaklı projeyle filizlenen bu çalışmanın, alanda yapılan üretimlere ışık tutacağını umuyoruz.

Türkiye'de özellikle gençlik alanı çok bakir olduğundan, internet çalışmalarıysa henüz 10'lu yaşlarına bile girmediğinden, işimiz oldukça zordu. Türkiye'de yaşayan gençlerin renkli sanal alem tasavvuru ve mesleki deformasyondan kaynaklanan kamuoyuyla paylaşım sorumluluğu, ayakta kalmamızı sağladı. Derlemeye, hem Türkiye özelinde hem de Türkiye'yi ilgilendiren konularda karşılaştırmalı proje bulgularını dahil ettik. Tabii bunun yanında, alanda çalışan uzman ve akademisyenlerin makalelerine ve röportajlarına da yer verdik. Makalelerin özelikle farklı mesleklere sahip ve alanda çalışan kişilerin elinden çıkmasını istedik. Akademisyen, danışman, yazar, gençlik örgütü çalışanı, sendikacı, gençlik çalışmaları yapan ve sivil toplum deneyimi olanlar... Röportajlara yer vermeye çalıştık çünkü bazı deneyime dayalı ifadelerin makale metninde kaybolabileceğini düşündük. Röportaj verenlerin profilinde de çeşitlilik olmasına dikkat ettik: Türkiye'nin ilk internet girişimcileri, yayıncı ve gençlik alanında uzun yıllardır çalışan eski gönüllü, genç profesyonel, genç akademisyen/blogçu.

İstanbul Şehir Üniversitesi öğretim üyelerinden **Yrd. Doç. Dr. Aslı Telli Aydemir**'in derlediği bu Alternatif Bilişim e-yayını, Kuzey Afrika ve Ortadoğu'da son dönemde yaşanan sosyal hareketlerle bir kat daha önem kazanan çevrim-içi/çevrim-dışı katılım dinamiklerine farklı bir pencereden bakmak isteyenlere önemle duyurulur.

*Katılımın "e-hali"*
*Gençlerin Sanal Alemi*
Derleme, Kasım 2011, 430 Sayfa
**Derleyen**: Aslı Telli Aydemir
**Yayına hazırlayan**: Işık Barış Fidaner
**İllüstrasyon**: Murat Ağdemir
Yazıların hakları yazarlara aittir.
Kitabın LaTeX kodları CC Attribution-NonCommercial 3.0 Unported License altındadır.

##İçindekiler

Teşekkürler

Neden Gençlerin Sanal Alemi? Bu kitabın çıkış sorunsalı / **Aslı Telli Aydemir**

###Bölüm I: Gençlik ve gençlik söylemi

    Türkiye’de kamusal söylemde gençlik kurgusunun değişimi / Leyla Neyzi
    Bilişim Teknolojileriyle Örgütlenen Gençlik Hareketleri ve Yeni Bir Siyaset Arayışı / Demet Lüküslü
    ‘80 sonrası Öğrenci Muhalefeti güncesi: Üniversiteler ve Genç-sen / Emrah Altındiş

###Bölüm II: Gençliğin sivil ve politik katılımına dair sorunlar

    Türkiye’de Gençlik Örgütlenmelerinin Yasal Düzenlemelere Katkısı / Başak Saral
    Gençlik, Bilgi Toplumu, Yönetişim, Sansür ve Türkiye / Özgür Uçkan
    İnsani Gelişmeyi Anlamak için Bilgi Çağından Yararlanmak Şart / Aygen Aytaç
    Röportaj / Evren Ergeç

###Bölüm III: Türkiye’de gençliğin politik katılımında yeni bir kanal: Internet

    Yeni Toplumsal İletişim Ethos’u: İstanbul Gençliği ve İletişim Teknolojileri Kullanımı / Halil Nalçaoğlu
    Türkiye’de Gençlik Merkezleri ve İnternet Siteleri / Gökdağ Göktepe ve Yörük Kurtaran
    Türkiye’de Gençliğin İnternet Üzerinden Katılımı Özelinde Genç Siyasallığı ve Müzakereci Demokrasi / Sezgin Çebi ve Yelda Şahin Akıllı
    Türkiye’de Barış Hareketinde İnternetin Kullanımı: Küresel BAK Örneği / Gülüm Şener
    Röportaj / Ersan Özer
    Röportaj / Mustafa Arslantunalı

###Bölüm IV: Genç Sanal Topluluklar ve yeni katılım biçimleri

    Gerçek zaman / sanal etkileşimi: Toplumsal paylaşım ağları , işlevsel katılım odakları ve sivil yeterlik / Aslı Telli Aydemir
    Dijital Oyun Kültürü Haritasında Oyuncular: Dijital Oyuncuların Habitusları ve Kariyer Türevleri / Mutlu Binark ve Günseli Bayraktutan
    Blogging ve Gençliğin Internet üzerinden Katılım süreçleri üzerine bir deneme / Erkan Saka
    Röportaj / Metin Solmaz
    Röportaj / Sedat Kapanoğlu
    Röportaj / Avi Haligua

###Bölüm V: DEĞERLENDİRME ve SONUÇ

    Değerlendirme ve Sonuç / Aslı Telli Aydemir

###Bölüm VI: Metinlerin ingilizce özetleri

    Abstracts in English

###Ekler

    Yazarlar Hakkında
